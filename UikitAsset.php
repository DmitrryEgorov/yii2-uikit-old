<?php

namespace egorov\uikit2;

use yii\web\AssetBundle;

class UikitAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/uikit.min.css',
    ];

    public $js = [
        'js/uikit.min.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];


}