UI Kit 2.27.5 Extension
==================
UI Kit 2 (2.27.5) Extension for Yii2  [UI kit Official site](https://getuikit.com/v2/)
Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist egorov/yii2-uikit-old "*"
```

or add

```
"egorov/yii2-uikit-old": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by:

Common:
```
<?php \egorov\uikit2\UikitAsset::register($this); ?>
```

Dynamic Grid:
```
<?php \egorov\uikit2\components\UikitDynamicGridAsset::register($this); ?>
```

Parallax Grid:
```
<?php \egorov\uikit2\components\UikitParallaxGridAsset::register($this); ?>
```

Dotnav:
```
<?php \egorov\uikit2\components\UikitDotnavAsset::register($this); ?>
```

Slidenav:
```
<?php \egorov\uikit2\components\UikitSlidenavAsset::register($this); ?>
```

Dynamic Pagination:
```
<?php \egorov\uikit2\components\UikitDynamicPaginationAsset::register($this); ?>
```

Form advanced:
```
<?php \egorov\uikit2\components\UikitFormAdvancedAsset::register($this); ?>
```

Form file:
```
<?php \egorov\uikit2\components\UikitFormFileAsset::register($this); ?>
```

Form password:
```
<?php \egorov\uikit2\components\UikitFormPasswordAsset::register($this); ?>
```

Form select:
```
<?php \egorov\uikit2\components\UikitFormSelectAsset::register($this); ?>
```

Placeholder:
```
<?php \egorov\uikit2\components\UikitPlaceholderAsset::register($this); ?>
```

Progress:
```
<?php \egorov\uikit2\components\UikitProgressAsset::register($this); ?>
```

Lightbox:
```
<?php \egorov\uikit2\components\UikitLightboxAsset::register($this); ?>
```

Autocomplete:
```
<?php \egorov\uikit2\components\UikitAutocompleteAsset::register($this); ?>
```

Datepicker:
```
<?php \egorov\uikit2\components\UikitDatepickerAsset::register($this); ?>
```

HTML editor:
```
<?php \egorov\uikit2\components\UikitHtmleditorAsset::register($this); ?>
```

Slider:
```
<?php \egorov\uikit2\components\UikitSliderAsset::register($this); ?>
```

Slideset:
```
<?php \egorov\uikit2\components\UikitSlideSetAsset::register($this); ?>
```

Slideshow:
```
<?php \egorov\uikit2\components\UikitSlideShowAsset::register($this); ?>
```

Parallax:
```
<?php \egorov\uikit2\components\UikitParallaxAsset::register($this); ?>
```

Accordion:
```
<?php \egorov\uikit2\components\UikitAccordionAsset::register($this); ?>
```

Notify:
```
<?php \egorov\uikit2\components\UikitNotifyAsset::register($this); ?>
```

Search:
```
<?php \egorov\uikit2\components\UikitSearchAsset::register($this); ?>
```

Nestable:
```
<?php \egorov\uikit2\components\UikitNestableAsset::register($this); ?>
```

Sortable:
```
<?php \egorov\uikit2\components\UikitSortableAsset::register($this); ?>
```

Sticky:
```
<?php \egorov\uikit2\components\UikitStickyAsset::register($this); ?>
```

Timepicker:
```
<?php \egorov\uikit2\components\UikitTimepickerAsset::register($this); ?>
```

Tooltip:
```
<?php \egorov\uikit2\components\UikitTooltipAsset::register($this); ?>
```

Upload:
```
<?php \egorov\uikit2\components\UikitUploadAsset::register($this); ?>
```








