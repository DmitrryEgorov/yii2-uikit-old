<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitSlideShowAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/slideshow.css',
    ];

    public $js = [
        'js/components/slideshow.js',
        'js/components/slideshow-fx.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}