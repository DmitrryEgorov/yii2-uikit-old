<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitStickyAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/sticky.css',
    ];

    public $js = [
        'js/components/sticky.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}