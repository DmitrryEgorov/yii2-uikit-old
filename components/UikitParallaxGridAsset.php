<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitParallaxGridAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
    ];

    public $js = [
        'js/components/grid-parallax.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}