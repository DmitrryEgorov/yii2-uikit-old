<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitHtmleditorAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/htmleditor.css',
    ];

    public $js = [
        'js/components/htmleditor.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}