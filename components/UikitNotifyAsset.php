<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitNotifyAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/notify.css',
    ];

    public $js = [
        'js/components/notify.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];
}