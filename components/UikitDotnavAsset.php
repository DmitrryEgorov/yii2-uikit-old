<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitDotnavAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/dotnav.css',
    ];

    public $js = [
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}