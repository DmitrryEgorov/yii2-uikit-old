<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitNestableAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/nestable.css',
    ];

    public $js = [
        'js/components/nestable.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}