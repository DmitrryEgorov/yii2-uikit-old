<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitSearchAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/search.css',
    ];

    public $js = [
        'js/components/search.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}