<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitFormFileAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/form-file.css',
    ];

    public $js = [
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}