<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitSlideSetAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
    ];

    public $js = [
        'js/components/slideset.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}