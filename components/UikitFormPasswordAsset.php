<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitFormPasswordAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/form-password.css',
    ];

    public $js = [
        'js/components/form-password.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}