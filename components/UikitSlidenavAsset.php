<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitSlidenavAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/slidenav.css',
    ];

    public $js = [
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}