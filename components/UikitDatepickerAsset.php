<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitDatepickerAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/datepicker.css',
    ];

    public $js = [
        'js/components/datepicker.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}