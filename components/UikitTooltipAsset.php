<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitTooltipAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/tooltip.css',
    ];

    public $js = [
        'js/components/tooltip.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}