<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitFormSelectAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/form-select.css',
    ];

    public $js = [
        'js/components/form-select.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}