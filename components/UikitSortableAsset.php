<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitSortableAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/sortable.css',
    ];

    public $js = [
        'js/components/sortable.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}