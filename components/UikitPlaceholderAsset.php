<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitPlaceholderAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/placeholder.css',
    ];

    public $js = [
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}