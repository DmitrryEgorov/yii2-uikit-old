<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitAutocompleteAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/autocomplete.css',
    ];

    public $js = [
        'js/components/autocomplete.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}