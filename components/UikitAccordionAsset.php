<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitAccordionAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
        'css/components/accordion.css',
    ];

    public $js = [
        'js/components/accordion.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];



}