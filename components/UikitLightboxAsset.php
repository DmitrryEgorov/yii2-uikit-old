<?php

namespace egorov\uikit2\components;

use yii\web\AssetBundle;

class UikitLightboxAsset extends AssetBundle
{
    public $sourcePath = '@vendor/egorov/yii2-uikit-old/dist';

    public $css = [
    ];

    public $js = [
        'js/components/lightbox.js'
    ];

    public $depends = [
        'egorov\uikit2\UikitAsset',
    ];


}